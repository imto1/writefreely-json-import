import json
import requests

url = 'https://example.com/api'
alias = 'USERNAME'
password = 'PASSWORD'
file_path = '/path/to/file.json'

# Get AccessToken
response = requests.post(f'{url}/auth/login', json={'alias': alias, 'pass': password})
json_response = response.json()
access_token = json_response['data']['access_token']

with open(file_path, 'rt') as file:
    data = json.load(file)
    # Posts
    posts = data['collections'][0]['posts']
    for post in posts:
        response = requests.post(f'{url}/collections/{alias}/posts',
                                 headers={'Authorization': f'Token {access_token}'},
                                 json={
                                     'body': post['body'],
                                     'title': post['title'],
                                     'lang': post['language'],
                                     'rtl': post['rtl'],
                                     'created': post['created'],
                                 })
        json_response = response.json()
        print(json_response['code'])

    # Drafts
    for post in data['posts']:
        response = requests.post(f'{url}/posts',
                                 headers={'Authorization': f'Token {access_token}'},
                                 json={
                                     'body': post['body'],
                                     'title': post['title'],
                                     'lang': post['language'],
                                     'rtl': post['rtl'],
                                     'created': post['created'],
                                 })
        json_response = response.json()
        print(json_response['code'])

# Logout    
response = requests.delete(f'{url}/auth/me',
                           headers={'Authorization': f'Token {access_token}'})
json_response = response.json()
print(json_response['code'])